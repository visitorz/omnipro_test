<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Block\Form;

class Index extends \Magento\Framework\View\Element\Template
{

    /**   
     * @var \Omnipro\BlogTest\Helper\Data 
     */
    protected  $dataHelper;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Magento\Framework\View\Element\Template\Context
     */  
    protected $context;  
    
    /**
     * @var \Omnipro\BlogTest\Model\ResourceModel\Listing\CollectionFactory
     */
    protected $postCollectionFactory;
    
    /**
     * @var \Omnipro\BlogTest\Model\ResourceModel\Attachment\CollectionFactory
     */
    protected $attachmentCollectionFactory; 
    

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,                
        \Omnipro\BlogTest\Helper\Data $dataHelper, 
        \Omnipro\BlogTest\Model\ResourceModel\Listing\CollectionFactory $postCollectionFactory,
        \Omnipro\BlogTest\Model\ResourceModel\Attachment\CollectionFactory $attachmentCollectionFactory
    ) {
        $this->dataHelper = $dataHelper; 
        $this->postCollectionFactory = $postCollectionFactory;
        $this->attachmentCollectionFactory = $attachmentCollectionFactory;
        parent::__construct($context);
    }
    
    /**
     * Get the post collection
     * @return object
     */
    public function getPostCollection()
    {
        $collection = $this->postCollectionFactory->create();

        return $collection;
    }  
    
    /**
     * Get the attachment collection
     * @return object
     */
    public function getAttachmentCollection()
    {
        $collection = $this->attachmentCollectionFactory->create();

        return $collection;
    } 
    
    /**
     * Get the blog title
     * @return string
     */
    public function getBlogTitle() 
    {
     return $this->dataHelper->getBlogTitle();  
    }
    
    /**
     * Get the blog sub-title
     * @return string
     */
    public function getBlogSubTitle() 
    {
        return $this->dataHelper->getBlogSubTitle();
    }
    
    protected function _toHtml() 
    {  
        if (!$this->dataHelper->isModuleEnable())
        {
           return ''; 
        }
        
        return parent::_toHtml();
    }
}