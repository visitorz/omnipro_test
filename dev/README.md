## Development test for Senior E-commerce Developer (PHP) at OMNI.PRO

## Introduction
With this test we want to evaluate your skills in the following areas:

* Optimal use of PHP
* Optimal use of MySQL
* Optimal use of Javascript and preferred libraries
* Medium use of HTML
* Medium use of CSS
* Medium use of Git
* Medium use of XML
* Programming best practices

During this test you can follow your personal development process. Please keep in mind that we want to get a product that balances quality and quantity, if you're not capable of finishing this test in its entirety in the given timeframe we still should be able to appreciate your work and skills. Think about this small application as a project that will be maintained and possibly extended in the future by other developers.

## Development
Build a simple Magento 2 module with blog functionality, this module will consist of just one single responsive page (customer frontend). The page's structure can be appreciated in the wireframes wf1.jpg, wf2.jpg, wf3.jpg. The wireframes also indicate the viewports that have to be implemented.

This blog should have a form for new posts that doesn't refresh the page. The page should show all the stored posts ordered from the most recent to the oldest.

If the user refreshes the page the information already stored must be preserved.

The form should have the following fields:
* Title
* Content
* Image
* Email address

It should not be possible to save the post if:
* The email address is invalid or empty
* The title is empty
* Both the content and the image are empty

In case of a validation error the user must receive feedback and the post should not be saved. While the form is being saved the user should be informed of the process, provide some animated feedback.

After submitting the post the PHP backend will run another validation round:
* It will verify that the email address is equal to a Magento user email that holds the Administrator role.
* It will verify that the post was sent from an authorized source, in other words, the blog's frontend.

After validating the post:
* Add a timestamp
* Create the corresponding HTML markup and return it to the frontend.
* Show the post in the frontend as the most recently created

Fron the Magento admin panel
* The existing posts should be visualized
* The posts can be deleted

A concise and documented Git history is expected. When the development has been finished a Merge Request should be created in order to notify the technical team that the test is ready to be evaluated. It is only necessary to version the directory that holds the module and not the Magento application. The extension should be auto-installable with the command "magento setup:upgrade".

### Bonus
* Build the post submit feedback using CSS only

Good luck!