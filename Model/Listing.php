<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Model;

use Magento\Framework\Model\AbstractModel;

class Listing extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'omnipro_blogtest_listing';
    
    /**
     * @var string
     */
    protected $attachment = null; 
    
    /**   
     * @var \Omnipro\BlogTest\Model\AttachmentFactory 
     */
    protected $attachmenttFactory;

    /**
     * @var \Omnipro\BlogTest\Helper\Data 
     */
    protected   $dataHelper;
    
    /**
     * 
     * @param \Omnipro\BlogTest\Model\AttachmentFactory $attachmenttFactory
     * @param \Omnipro\BlogTest\Helper\Data $dataHelper
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Omnipro\BlogTest\Model\AttachmentFactory $attachmenttFactory,
        \Omnipro\BlogTest\Helper\Data $dataHelper,         
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->attachmenttFactory = $attachmenttFactory;
        $this->dataHelper = $dataHelper;
        $this->localeDate = $localeDate;
        $this->context = $context;
        $this->registry = $registry;
        $this->resource = $resource;
        $this->resourceCollection = $resourceCollection;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }  
    
    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }     
    
    protected function _construct()
    {
        $this->_init('Omnipro\BlogTest\Model\ResourceModel\Listing');
    }
    
    /**
     * Return the attachment
     * @return blob
     */
    public function getAttachment() 
    { 
        if (!$this->attachment) 
        {
            $attachment = $this->attachmenttFactory->create()->load($this->getBpAttachmentId());
            
            $this->attachment = $attachment;
        }

        return $attachment->getBody();       
    }        
}