<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Model\ResourceModel\Listing;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{    
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    
    protected function _construct()
    {
        $this->_init(
            'Omnipro\BlogTest\Model\Listing',
            'Omnipro\BlogTest\Model\ResourceModel\Listing'
        );
    }
    
    /**
     * 
     * @return $collection
     */
    public function joinFields()
    {
        $select = $this->getSelect();
        $select->joinLeft(
            ['attachment' => $this->getTable('omnipro_blogtest_attachment')],
            'main_table.bp_attachment_id = attachment.attachment_id',
            ['attachment' => 'name']
        );
        
        return $this;
    }
}

