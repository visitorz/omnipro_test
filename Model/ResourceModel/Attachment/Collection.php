<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Model\ResourceModel\Attachment;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{        
    protected function _construct()
    {
        $this->_init(
            'Omnipro\BlogTest\Model\Attachment',
            'Omnipro\BlogTest\Model\ResourceModel\Attachment'
        );
    } 
}