<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Model\ResourceModel;

class Attachment extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{  
    /**
     * 
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param type $resourcePrefix
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        $resourcePrefix = null
    ) {
        parent::__construct($context, $resourcePrefix);
    }
  
    protected function _construct()
    {
        $this->_init('omnipro_blogtest_attachment', 'attachment_id');
    }   
}