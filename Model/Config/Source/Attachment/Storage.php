<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Model\Config\Source\Attachment;

class Storage implements \Magento\Framework\Option\ArrayInterface
{    
    const STORAGE_FS = 'fs';
    const STORAGE_DB = 'db';
    
    /**
     * @return array
     */
    public function toArray()
    {
        return [
           // Config::ATTACHMENT_STORAGE_FS => __('File System'),
            self::STORAGE_DB => __('Database'),
        ];
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];
        foreach ($this->toArray() as $k => $v) {
            $result[] = ['value' => $k, 'label' => $v];
        }

        return $result;
    }
}