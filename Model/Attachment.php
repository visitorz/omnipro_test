<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Model;

use Magento\Framework\Model\AbstractModel;

class Attachment extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'omnipro_blogtest_attachment';
    
    const ATTACHMENT_STORAGE_FS = 'fs';
    const ATTACHMENT_STORAGE_DB = 'db';   
    
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlManager;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;

    /**
     * @var \Magento\Framework\Model\Context
     */
    protected $context;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\Model\ResourceModel\AbstractResource
     */
    protected $resource;
    
    
    /**
     * @var \Omnipro\BlogTest\Helper\Data 
     */
    protected  $dataHelper;    
    
    /**
     * 
     * @param \Magento\Framework\UrlInterface $urlManager
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Omnipro\BlogTest\Helper\Data $dataHelper,
        \Magento\Framework\UrlInterface $urlManager,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->dataHelper = $dataHelper;
        $this->urlManager = $urlManager;
        $this->filesystem = $filesystem;
        $this->context = $context;
        $this->registry = $registry;
        $this->resource = $resource;
        $this->resourceCollection = $resourceCollection;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
    
    protected function _construct()
    {
        $this->_init('Omnipro\BlogTest\Model\ResourceModel\Attachment');
    }
        
    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    /**
     * @todo will be used for the file system storage functionality
     */
    public function getAttachmentFolderPath(){}
    public function getExternalPath(){}
    
    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->urlManager->getUrl('blogtest/form/attachment', ['id' => $this->getAttachmentId()]);
    }
    public function getBody() 
    {
        if($this->dataHelper->getAttachamentStorageType() == self::ATTACHMENT_STORAGE_DB)
        {
            return $this->getData('body');
        } 
    }
    
    public function setBody($decodedContent)
    {
        if($this->dataHelper->getAttachamentStorageType() == self::ATTACHMENT_STORAGE_DB)
        {
            $this->setData('body', $decodedContent);
            $this->setStorage(self::ATTACHMENT_STORAGE_DB);
        }
        
        $this->save();

        return $this;
    }
    
   /**
     * @return string
     */
    public function getName()
    {
        if ($this->getData('name')) {
            return $this->getData('name');
        }

        return 'noNamePicture';
    }    
}
