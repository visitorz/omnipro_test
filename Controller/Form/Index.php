<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Controller\Form;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Omnipro\BlogTest\Controller\Form
{ 
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    { 
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        return $resultPage;
    }
}
