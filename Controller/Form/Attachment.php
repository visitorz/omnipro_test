<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Controller\Form;

use Magento\Framework\Controller\ResultFactory;

class Attachment extends \Omnipro\BlogTest\Controller\Form
{
    /**
     * Download attachments in frontend
     *
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Raw $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_RAW);

        $externalId = $this->getRequest()->getParam('id');
        $collection = $this->attachmentCollectionFactory->create()
            ->addFieldToFilter('attachment_id', $externalId);

        $attachment = $collection->getFirstItem();

        // Give to the picture the proper headers
        $resultPage->setHeader("Content-Disposition", "attachment; filename={$attachment->getName()}");
        $resultPage->setHeader("Content-length", $attachment->getSize());
        $resultPage->setHeader("Content-type", $attachment->getType());
        $resultPage->setContents($attachment->getBody());
        return $resultPage;
    }
}
