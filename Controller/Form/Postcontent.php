<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Controller\Form;

use Magento\Framework\Controller\ResultFactory;

class Postcontent extends \Omnipro\BlogTest\Controller\Form
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {   
        $post = $this->_initPost(); 
        
        if(!$post)
        {
            $this->postContent();
            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        }
        return $resultRedirect->setPath('*/*/');        
    }
}
