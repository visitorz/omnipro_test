<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Controller;

abstract class Index extends \Magento\Framework\App\Action\Action
{       
    /**
     * @var \Magento\Framework\App\Action\Context
     */
    protected $context;

    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context
    ) {
        $this->context = $context;
        $this->resultFactory = $context->getResultFactory();      
        parent::__construct($context);
    }     
}