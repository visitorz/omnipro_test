<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Controller\Adminhtml\Listing;

use Magento\Framework;

class Index extends \Magento\Backend\App\Action
{   
    /**
    *
    * @var Omnipro\BlogTest\Helper\Data
    */
    protected  $dataHelper;
    
    protected $resultPageFactory = false;
    
    /**
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Omnipro\BlogTest\Helper\Data $dataHelper
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,          
        \Omnipro\BlogTest\Helper\Data $dataHelper,   
        \Magento\Framework\View\Result\PageFactory $resultPageFactory      
    ) {
        parent::__construct($context);       
        $this->dataHelper = $dataHelper;        
        $this->resultPageFactory = $resultPageFactory;
    }
    
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Omnipro_BlogTest::blogtest');
        $resultPage->addBreadcrumb(__('Omnipro Blog Test - Post Listing'), __('Omnipro Blog Test - Post Listing'));
        $resultPage->getConfig()->getTitle()->prepend(__('Omnipro Blog Test - Post Listing'));
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Omnipro_BlogTest::blogtest_listing');
    }
}