<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Controller\Adminhtml\Listing;

use Magento\Framework;

class Refresh extends \Magento\Backend\App\Action
{   
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory = false;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,          
        \Magento\Framework\View\Result\PageFactory $resultPageFactory                    
    ) {

        parent::__construct($context);     
        $this->resultPageFactory = $resultPageFactory;
    }
    
    public function execute()
    {        
        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Omnipro_BlogTest::blogtest_listing');
    }
}