<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Controller;

abstract class Form extends \Magento\Framework\App\Action\Action
{       
    /**
     * @var \Magento\Framework\App\Action\Context
     */
    protected $context;

    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;
    
    /**
     * @var \Omnipro\BlogTest\Model\ResourceModel\Listing\CollectionFactory
     */
    protected $postCollectionFactory; 
    
    /**
     *
     * @var \Omnipro\BlogTest\Helper\Data 
     */
    protected  $dataHelper; 
        
    /**
     * @var \Omnipro\BlogTest\Model\ResourceModel\Attachment\CollectionFactory
     */
    protected $attachmentCollectionFactory;     
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Omnipro\BlogTest\Model\ResourceModel\Listing\CollectionFactory $postCollectionFactory,
        \Omnipro\BlogTest\Model\ResourceModel\Attachment\CollectionFactory $attachmentCollectionFactory,    
        \Omnipro\BlogTest\Helper\Data $dataHelper             
    ) {
        $this->context = $context;
        $this->resultFactory = $context->getResultFactory();
        $this->postCollectionFactory = $postCollectionFactory;
        $this->attachmentCollectionFactory = $attachmentCollectionFactory; 
        $this->dataHelper = $dataHelper;        
        parent::__construct($context);
    }
    
   /**
     * @return \Omnipro\BlogTest\Model\Listing
     */
    protected function _initPost()
    {
        if ($id = $this->getRequest()->getParam('id')) 
        {
            $post = $this->postCollectionFactory->create()
              ->joinFields()
              ->addFieldToFilter('main_table.id', $id)
              ->getFirstItem();
            
            if ($post->getId() > 0) 
            {
                $this->registry->register('current_post', $post);

                return $post;
            }
        }
    }

    protected function postContent()
    {
        $email = $this->getRequest()->getParam('email');
        
        $hasUserAdminRole = $this->dataHelper->hasUserAdminRole($email);
        
        if(!$hasUserAdminRole)
        {
            $this->messageManager->addErrorMessage("Sorry, you will need administrator rights to post here!");
            return;
        }
        
        $title = $this->dataHelper->cleanInput($this->getRequest()->getParam('title'));        
        $content = $this->dataHelper->cleanInput($this->getRequest()->getParam('content'));
        
        $hasPostImage = isset($_FILES['attachment']) 
                && $_FILES['attachment']['type'][0] 
                && $_FILES['attachment']['name'][0] != '';
                
        if(!$hasPostImage || !$this->dataHelper->isAuthorizedFileExt($_FILES['attachment']['type'][0]))
        {
            $this->messageManager->addErrorMessage(__('You have a wrong attachement file'));
            return;
        }
        
        try {                    
                if ($email && $title && $content && $hasPostImage)
                { 
                    $this->_submitPost($email, $title, $content);
                }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } 
    } 
    
    /**
     * 
     * @param string $email
     * @param string $title
     * @param string $content
     */
    protected function _submitPost($email, $title, $content)
    {
        // This save the attachment picture
        $attachmentId = $this->dataHelper->saveAttachments();
        // This create the post
        $this->dataHelper->createPost($email, $title, $content,$attachmentId);
        $this->messageManager->addSuccessMessage(__('Your post was successfuly posted'));
    }
}
