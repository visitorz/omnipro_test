<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Omnipro\BlogTest\Controller\Index
{ 
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    { 
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('blogtest/form');

        return $resultRedirect;
    }
}
