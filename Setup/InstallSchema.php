<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * @package Omnipro\BlogTest\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        
        /**
         * Create table 'omnipro_blogtest_attachment'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('omnipro_blogtest_attachment')
        )->addColumn(
            'attachment_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => false, 'nullable' => false, 'identity' => true, 'primary' => true],
            'Attachment Id'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['unsigned' => false, 'nullable' => false],
            'Picture Name'
        )->addColumn(
            'type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['unsigned' => false, 'nullable' => false],
            'Type'
        )->addColumn(
            'size',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => false, 'nullable' => true],
            'Size'
        )->addColumn(
            'body',
            \Magento\Framework\DB\Ddl\Table::TYPE_BLOB,
            '4G',
            ['unsigned' => false, 'nullable' => true],
            'Body' 
            /**
             * @todo Will be used for allowing to save attached image in file       
        )->addColumn(
            'external_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['unsigned' => false, 'nullable' => false],
            'External Id' */
        )->addColumn(
            'storage',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['unsigned' => false, 'nullable' => true],
            'Storage'
        );
        $installer->getConnection()->createTable($table);
        

        /**
         * Create table 'omnipro_blogtest_post'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('omnipro_blogtest_post')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Post ID'
        )->addColumn(
            'title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Post Title'
        )->addColumn(
            'content',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            ['nullable' => false],
            'Post Content'
        )->addColumn(
            'email',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Post Title'                
        )->addColumn(
            'create_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [],
            'Post Creation Time'
        )->addColumn(
            'bp_attachment_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => false, 'nullable' => false],
            'Attachment Id' 
        )->addForeignKey(
            $installer->getFkName('omnipro_blogtest_post', 'bp_attachment_id', 'omnipro_blogtest_attachment', 'attachment_id'),
                'bp_attachment_id',
                $installer->getTable('omnipro_blogtest_attachment'),
                'attachment_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE                                 
        )->setComment(
            'Omnipro BlogTest main Table'
        );
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}
