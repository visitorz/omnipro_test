<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper 
{   
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;    /**
     * @var \Magento\Framework\App\Helper\Context
     */
    protected $context;
    
    /**
     * @var \Omnipro\BlogTest\Helper\Config 
     */
    protected  $configHelper;
    
    /**
     * @var \Omnipro\BlogTest\Model\ListingFactory
     */
    protected $postFactory; 
    
    /**
     * string
     */
    protected $attachmentStorateType;
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $date; 
    
   /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager; 
    
    /**
     * @var \Omnipro\BlogTest\Model\AttachmentFactory
     */
    protected $attachmentFactory;

    /**
     * @var string 
     */
    protected $isModuleEnable;
    
    /**  
     * @var string
     */
    protected $blogTitle;
    
    /**   
     * @var string 
     */
    protected $blogSubTitle;


    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Omnipro\BlogTest\Model\ListingFactory $postFactory, 
        \Omnipro\BlogTest\Model\AttachmentFactory $attachmentFactory, 
        \Magento\Framework\Stdlib\DateTime\DateTime $date, 
        \Omnipro\BlogTest\Helper\Config $configHelper,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) 
    {
        $this->logger = $context->getLogger();
        $this->postFactory = $postFactory;
        $this->attachmentFactory = $attachmentFactory;        
        $this->configHelper = $configHelper; 
        $this->date = $date;
        $this->objectManager = $objectManager;
        parent::__construct($context);    
    }
    
    /**
     * Is the module enabled
     * @return string
     */
    public function isModuleEnable() 
    {
        if($this->isModuleEnable)
        {
           return $this->isModuleEnable;
        }
        
        $isModuleEnable = $this->configHelper->isEnabled();
        $this->isModuleEnable = $isModuleEnable;
        
        return $isModuleEnable;
    }
    
    /**
     * Get the blog title
     * @return string
     */
    public function getBlogTitle() 
    {
        if($this->blogTitle)
        {
            return $this->blogTitle;
        }
        
        $blogTitle = $this->configHelper->getBlogTitle();
        $this->blogTitle = $blogTitle;
        
        return $blogTitle;
    }
    
    /**
     * Get the blog sub-title
     * @return string
     */
    public function getBlogSubTitle() 
    {
        if($this->blogSubTitle)
        {
            return $this->blogSubTitle;
        }
        
        $blogSubTitle = $this->configHelper->getBlogSubTitle();
        $this->blogSubTitle = $blogSubTitle;
        
        return $blogSubTitle;
    }
    
    /**
     * Get the attachment storage type
     * @return string
     */
    public function getAttachamentStorageType() 
    {
        if($this->attachmentStorateType)
        {
           return $this->attachmentStorateType;
        }
        
        $attachmentStorateType = $this->configHelper->getAttachmentStorateType();
        $this->attachmentStorateType = $attachmentStorateType;
        
        Return $attachmentStorateType;
    }
    
    public function createPost($email, $title, $content, $attachmentId)
    {        
        $post = $this->postFactory->create();
        $post->setTitle($title)
            ->setContent($content)
            ->setEmail($email)
            ->setCreateAt($this->date->gmtDate())
            ->setbpAttachmentId($attachmentId);
        $post->save();
        
        return $post;
    }   
    
    /**
     * Save the attachment
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException | 
     */
    public function saveAttachments()
    {
        if (!isset($_FILES['attachment']['name'])) {
            return;
        }
        $maxSize = (int) ($this->_fileUploadMaxSize() / 1000000);
        $i = 0;

        foreach ($_FILES['attachment']['name'] as $name) {
            
            if ($name == '') 
            {
                continue;
            }
            
            if ($_FILES['attachment']['tmp_name'][$i] == '')
            {
                throw new \Magento\Framework\Exception\LocalizedException(
                    "Can't upload file $name . Max allowed upload size is ".$maxSize.' MB.'
                );
            }
            //@fixme - need to check for max upload size and alert error
            $body = file_get_contents(addslashes($_FILES['attachment']['tmp_name'][$i]));
            //create and save attachment
            $attachment = $this->attachmentFactory->create()
                            ->setName($name)
                            ->setType(strtoupper($_FILES['attachment']['type'][$i]))
                            ->setSize($_FILES['attachment']['size'][$i])
                            ->setBody($body)
                            ->setStorage($this->getAttachamentStorageType())    
                            ->save();
            ++$i;            
        }
        
        return $attachment->getAttachmentId();
    }  
    
    /**
     * Get max upload size in bytes.
     * @return float
     */
    protected function _fileUploadMaxSize()
    {
        static $maxSize = -1;
        if ($maxSize < 0) {
            $maxSize = $this->_parseSize(ini_get('post_max_size'));
            $uploadMax = $this->_parseSize(ini_get('upload_max_filesize'));
            if ($uploadMax > 0 && $uploadMax < $maxSize) {
                $maxSize = $uploadMax;
            }
        }
        return $maxSize;
    } 
    
    /**
     * @param int $size
     * @return float
     */
    protected function _parseSize($size)
    {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
        $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
        if ($unit) {
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        } else {
            return round($size);
        }
    } 
    
    /**
     * Has the user administrator role
     * @param $email
     * @return boolean
     */
    public function hasUserAdminRole($email) 
    {
        $userModel = $this->objectManager->create('Magento\User\Model\User');
        $userModel->load($email,'email');
        
        if($userModel->getId())
        {
           $roleName = $userModel->getRole()->getRoleName(); 
           
            if($roleName === 'Administrators')
            {
              return true;  
            }
            
            return false;
        }        
       
        return false;
    } 
    
    public function cleanInput($input) 
    {
        $search = array(
        '@<script[^>]*?>.*?</script>@si',  
        '@<[\/\!]*?[^<>]*?>@si',           
        '@<style[^>]*?>.*?</style>@siU',  
        '@<![\s\S]*?--[ \t\n\r]*>@'         
        );
        
        $output = preg_replace($search, '', $input);
        return $output;
    }
    
    public function isAuthorizedFileExt($file)
    {
        $allowed =  array('gif','png' ,'jpg', 'peg');
        
        if(substr($file, -3)){
           $ext = substr($file, -3); 
        }
        
        if(!$ext)
        {
            return false;
        }
        
        if(!in_array($ext, $allowed) ) 
        {
            return false;
        }
        
        return true;
    }
}
