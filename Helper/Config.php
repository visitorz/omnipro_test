<?php
/**
 * @category   Omnipro
 * @package    omnipro/module-blog-test
 * @author     issaberthe@gmail.com
 */

namespace Omnipro\BlogTest\Helper;

class Config extends \Magento\Framework\App\Helper\AbstractHelper {

    const XML_CONFIG_PATH = 'omnipro_blogtest/omnipro_blogtest_config/';
    
    protected $scopeConfig;
    
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(\Magento\Framework\App\Helper\Context $context) 
    {
        parent::__construct($context);
        $this->scopeConfig = $context->getScopeConfig();
    }

    /**
     * Is the module enabled
     * 
     * @return boolean
     */
    public function isEnabled() 
    {
        return $this->scopeConfig->getValue(
                        self::XML_CONFIG_PATH . 'activation', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    public function getBlogTitle() 
    {
        return $this->scopeConfig->getValue(
                        self::XML_CONFIG_PATH . 'blog_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);                
    }
    
    public function getBlogSubTitle() 
    {
        return $this->scopeConfig->getValue(
                        self::XML_CONFIG_PATH . 'blog_sub_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);                
    } 
    
    public function getAttachmentStorateType() {
        return $this->scopeConfig->getValue(
                         self::XML_CONFIG_PATH . 'attachment_storage', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);        
    }
}
